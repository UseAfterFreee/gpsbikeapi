from flask_restful import Resource, reqparse
from .. import db

import decimal
import datetime
import math

from ..models.BumpLevel import BumpLevel

def findCorner(lat, lon):
    lat = math.floor(float(lat) * 1e4)
    lon = math.floor(float(lon) * 1e3)

    lat = (lat - (lat % 5)) / 1e4
    lon = lon / 1e3
    return lat, lon

def isInTile(tlat, tlon, lat, lon):
    inLat = lat > tlat and lat < tlat + 0.0005
    inLon = lon > tlon and lon < tlon + 0.001

    return inLat and inLon

class BumplevelController(Resource):
    def get(self):
        bundle = True
        levels = BumpLevel.query.limit(500).all()
        if not bundle:
            return [bumplevel.as_dict() for bumplevel in levels]
        else:
            returnVals = []
            processedIds = []

            for level in levels:
                if level.id in processedIds:
                    continue

                lat, lon = findCorner(level.lat, level.lon)

                toBundle = [p for p in levels if isInTile(lat, lon, p.lat, p.lon)]
                amount = len(toBundle)
                total = 0
                for t in toBundle:
                    processedIds.append(t.id)
                    total += t.score

                returnVals.append(BumpLevel(lat=round(lat+0.00025,5), lon=round(lon+0.0005,5), score = total/amount, timestamp=level.timestamp))

            return [v.as_dict() for v in returnVals]
    
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('lat', type=decimal.Decimal)
        parser.add_argument('lon', type=decimal.Decimal)
        parser.add_argument('score', type=int)

        args = parser.parse_args()

        level = BumpLevel(lat=args['lat'], lon=args['lon'], score=args['score'], timestamp=datetime.datetime.now())
        db.session.add(level)
        db.session.commit()
        
        return level.as_dict()
