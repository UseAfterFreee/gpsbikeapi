from .. import db
import datetime

class BumpLevel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    lat = db.Column(db.DECIMAL(10,7), nullable=False)
    lon = db.Column(db.DECIMAL(10,7), nullable=False)
    score = db.Column(db.Integer, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)

    def as_dict(self):
        epoch = datetime.datetime.utcfromtimestamp(0)

        return {
            'id'    : self.id if self.id else 0,
            'lat'   : float(self.lat),
            'lon'   : float(self.lon),
            'score' : int(self.score),
            'timestamp' : (self.timestamp - epoch).total_seconds()
        }
